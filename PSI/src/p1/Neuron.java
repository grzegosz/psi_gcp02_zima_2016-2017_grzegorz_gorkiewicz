package p1;

import java.util.ArrayList;
import java.util.List;

public class Neuron {

	private ArrayList<Float> X;
	private ArrayList<Float> W;
	
	public Neuron(int n)
	{
		X = new ArrayList<>(n);
		W = new ArrayList<>(n);
		X.add(1f);
		W.add(0f);
	}
	
	public Neuron()
	{
		X = new ArrayList<>(2);
		W = new ArrayList<>(2);
		X.add(0f);
		W.add(1f);
	}
	
	public float MembranePotential()
	{
		float potential=0;
		if(X.isEmpty() && W.isEmpty())
			return -1;
		
		for(int i=0; i<X.size(); i++)
			potential +=X.get(i)*W.get(i);
		
		return potential;
	}
	
	public float Activation()
	{
		float y=0;
		if(MembranePotential()<0)
			y = 0;
		else if(MembranePotential()>0)
			y = 1;
		return y;
	}
}
